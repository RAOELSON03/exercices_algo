const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const routing = require('./routes');

app.use(express.json());
app.use('/api',routing);

app.get('/', (req, res) => {
    res.sendFile(__dirname+ '/index.html');
})


app.listen(PORT, () => {
    console.log(`server is running on port : ${PORT}`);
})