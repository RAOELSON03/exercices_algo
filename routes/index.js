const express = require('express');
const traitements = require('./traitements');
const router = express.Router();

router.post('/', (req, res, next) => {
    try {
      const body = req.body.nombre;
      const data = traitements.getRoman(body);
      res.json({
          'data': data
      });
    } catch(e) {
      next(e);
    }
});

module.exports = router;