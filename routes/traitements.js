module.exports.getRoman =  function (_number) {
    let number = _number;
    const basicRomanNumbers = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];
    const basicNumbers  = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];

    //a number > 4999 can't be write in roman numbers
    if (number > 4999) number = 4999;
    // in roman numbers 0 don't exist
    if (number <= 0) return "désolé il n\'y a pas ce nombre en chiffre romain";
    // decimal number can't be write in roman numbers
    number = parseInt(number);
    let romanNumber= '';
    let i = 0;
    while(number > 0){
        if(number >= basicNumbers[i]) 
        {
            number = number - basicNumbers[i];
            romanNumber = romanNumber+basicRomanNumbers[i];
        }
        else
        {
            i++;
        }
    }
    return romanNumber;
}